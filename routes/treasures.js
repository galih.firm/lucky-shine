var express = require('express');
var router = express.Router();
const treasures = require("../controllers/treasure.controller.js");

/* GET treasures. */
router.post('/inradius/', treasures.findByLocation);
router.post('/bigprize/', treasures.findBigPrizeValue);

module.exports = router;