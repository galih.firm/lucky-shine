'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Treasure extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // const Treasure = sequelize.define('Treasure', {});
      const MoneyValue = sequelize.define('MoneyValue', {}, {tableName: 'money_values', underscored: true});

      Treasure.hasMany(MoneyValue, { as: 'prize'});
    }
  };
  Treasure.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    latitude: DataTypes.DECIMAL(10,8),
    longitude: DataTypes.DECIMAL(11.8),
    name: DataTypes.STRING
  },
  {
    sequelize,
    modelName: 'Treasure',
  });
  return Treasure;
};