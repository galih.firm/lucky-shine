'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MoneyValue extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      const MoneyValue = sequelize.define('MoneyValues', {});
      const Treasure = sequelize.define('Treasures', {});

      MoneyValue.belongsToMany(Treasure, { as: 'prize', through: 'treasure_id'});
    };
  };

  MoneyValue.init({
    treasure_id: {
      type: DataTypes.INTEGER,
      foreignKey: true
    },
    amt: DataTypes.INTEGER
  }, 
  {
    sequelize,
    modelName: 'MoneyValue',
    tableName: 'money_values',
    underscored: true
  });

  MoneyValue.removeAttribute('id');
  return MoneyValue;
};