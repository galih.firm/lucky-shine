'use strict';

const models = require('../models/');
const Treasure = models.Treasure;
const MoneyValue = models.MoneyValue;
const geolib = require('geolib');
const _ = require('lodash');
const { Op } = require("sequelize");


exports.findByLocation = (req, res) => {
  let isAllowedDistance = req.body.distance == 1 || req.body.distance == 10 ? true : false;

  if(isAllowedDistance){
    let distance = req.body.distance * 1000;

    Treasure.findAll()
    .then(function(results){
      let inRadiusTreasures = [];

      _.each(results, function(result){
        let inRadius = geolib.isPointWithinRadius(
          { latitude: result.latitude, longitude: result.longitude },
          { latitude: req.body.latitude, longitude: req.body.longitude },
          distance
        );

        if(inRadius){
          inRadiusTreasures.push(result);
        }
      });

      res.send(inRadiusTreasures);
    }).catch(function(message){
      res.send({message: message.name || "Some error occurred while retrieving treasures."});
    });
  } else {
    res.send('Only 1 KM or 10 KM distance allowed!!');
  }

};

exports.findBigPrizeValue = (req, res) => {
  let isAllowedDistance = req.body.distance == 1 || req.body.distance == 10 ? true : false;
  let isPrizeAllowed = _.isInteger(req.body.prizeFrom) && _.isInteger(req.body.prizeTo) ? true : false;

  if(isAllowedDistance && isPrizeAllowed){
    let distance = req.body.distance * 1000;

    let prizeFrom = req.body.prizeFrom;
    let prizeTo = req.body.prizeTo;

    Treasure.findAll({
      include: [{
        model: MoneyValue,
        as: 'prize',
        where: { 
          amt: {[Op.between]: [prizeFrom, prizeTo]}
        }
      }]
    }).then(function(results){
      let inRadiusTreasures = [];

      _.each(results, function(point){

        let inRadius = geolib.isPointWithinRadius(
          { latitude: point.latitude, longitude: point.longitude },
          { latitude: req.body.latitude, longitude: req.body.longitude },
          distance
        );

        if(inRadius){
          inRadiusTreasures.push(point);
        }
      });

      res.send(inRadiusTreasures);
    }).catch(function(message){
      res.send({message: message.name || "Some error occurred while retrieving prizes."});
    });

  } else {
    res.send('Only 1 KM or 10 KM distance allowed and Prize Range are number !!');
  }
};